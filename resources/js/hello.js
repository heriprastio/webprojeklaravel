function calculate(begin, end)
{
    var result = 0;
    for (var i = begin; i <= end; i++)
    {
        result += i;
    }
    return result;  // return the result            
