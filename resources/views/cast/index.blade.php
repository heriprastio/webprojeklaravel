@extends('tampilan.layout')
@section('judul')
Data Cast
@endsection

@section('content')

<a href="/cast/create" class="btn btn-success mb-3">Tambah Data</a>

<table class="table">
    <thead class="thead-light">
        <tr>
            <th scope="col">No</th>
            <th scope="col">Nama</th>
            <th scope="col">Bio</th>
            <th scope="col">Action</th>
        </tr>
    </thead>
    <tbody>
        @forelse ($cast as $key=>$data)
        <tr>
            <td>{{$key +1}}</td>
            <td>{{$data->name}}</td>
            <td>{{$data->bio}}</td>
            <td>
                <form action="/cast/{{$data->id}}" method="post">
                    <a href="/cast/{{$data->id}}" class="btn btn-info">Detail</a>
                    <a href="/cast/{{$data->id}}/edit" class="btn btn-primary">Edit</a>
                    @csrf
                    @method('DELETE')
                    <input type="submit" class="btn btn-danger my-1" value="Delete">
                </form>
            </td>
        <tr>
            @empty
        <tr>
            <td>Tidak ada data</td>
        </tr>

        @endforelse
    </tbody>
</table>

@endsection