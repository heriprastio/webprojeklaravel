<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;600&display=swap" rel="stylesheet">

    <!-- Styles -->
    <style>
        html,
        body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Nunito', sans-serif;
            font-weight: 200;
            height: 100vh;
            margin: 0;
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }

        .content {
            text-align: justify;
            margin-left: 40px;
            margin-right: 40px;
            margin-top: 40px;
        }

        .title {
            font-size: 50px;
        }

        /* a {
            text-decoration: none !important;
        } */

        .links>a {
            color: #636b6f;
            padding: 0 25px;
            font-size: 13px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }

        .m-b-md {
            margin-bottom: 30px;
            margin-left: 30px;
        }
    </style>
</head>

<body>

    <div class="content">
        <h1>Buat Account Baru</h1>
        <form action="welcome" method="post">
            @csrf
            <label>Nama Pertama</label>
            <input type="text" name="namapertama"><br><br>
            <label>Nama Kedua</label>
            <input type="text" name="namakedua"><br><br>
            <label>Gender : </label>
            <br>
            <input type="radio" name="gender" value="male" required> Male
            <br>
            <input type="radio" name="gender" value="female" required> Female
            <br>
            <input type="radio" name="gender" value="other" required>Other
            <br>
            <br>
            <label> Nationality : </label>
            <br>
            <select id="nationality" name="nationality">
                <option value="indonesia">Indonesia</option>
                <option value="indonesia">United States</option>
                <option value="indonesia">Argentina</option>
            </select>
            <br>
            <br>
            <label> Bio :</label>
            <br>
            <textarea name="bio">

        </textarea>
            <br>
            <br>
            <!-- <input type="submit" value="kirim"> -->
            <button type="submit" value="submit">Sign Up
        </form>
    </div>

</body>

</html>